#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE DATABASE nlx_controller;
    CREATE DATABASE nlx_manager;
    CREATE DATABASE nlx_txlog;
    CREATE DATABASE nlx_auditlog;
EOSQL
