#!/bin/bash 
set -o errexit
set -o pipefail
set -o nounset

# Internal certs
echo "Generating internal certificates"
cd ./pki/internal/ca
cfssl gencert -initca ./csr-ca.json | cfssljson -bare ca
cfssl gencert -initca ./intermediate-ca.json | cfssljson -bare intermediate_ca
cfssl sign -ca ca.pem -ca-key ca-key.pem -config ../cfssl.json -profile intermediate_ca intermediate_ca.csr | cfssljson -bare intermediate_ca

cd ../certs
cfssl gencert -ca ../ca/intermediate_ca.pem -ca-key ../ca/intermediate_ca-key.pem -config ../cfssl.json -profile=server ./internal.json | cfssljson -bare internal-cert
cd ../../../

echo "Done generating internal certificates"
