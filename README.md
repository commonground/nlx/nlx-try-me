# NLX Try Me

This repository contains the files required to follow our [Try NLX guide](https://docs.fsc.nlx.io/try-fsc-nlx/docker/introduction).
After following this guide you will have a working NLX setup connected to our demo environment.

Please note that the NLX setup in this repository is made for demo purposes only. It should not be used as a production setup.

## Setup

### Generate internal and organization certificates
```bash
docker run --rm -it -v $(pwd):/workdir -w /workdir --entrypoint /bin/bash cfssl/cfssl:1.6.1 ./scripts/init-certs.sh
```

### Start NLX
```bash
docker-compose up -d
```
